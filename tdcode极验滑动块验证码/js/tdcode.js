/**
 * @author zhoujy
 * @description tdcode 1.0 - 仿极验离线验证码
 * @see zjysun@yeah.net
 * @copyright http://www.onst.com
 */
var $elementId; // 被点击元素ID，通过页面用户指定并传递
var $rootPath; // 图片加载根目录，根据部署方式路径用户自行决定

// 容错象素 越大体验越好，越小破解难道越高
var faultx = 2;

var bg_widthx = 240; // 拼图长
var bg_heightx = 150; // 拼图宽

var mark_widthx = 50;
var mark_heightx = 50;

var tdcode_offset; // 滑块坐标解
var tdcode_err; // 错误累计次数

var imgLibrary = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']; // 背景图片文件名列表，此处数字是图片文件名

var widthConfig = 240; // 图片宽度
var heightConfig = 150; // 图片高度
var plSize = 50; // 滑块的尺寸大小
var padding = 20; // 滑块在背景中的高度
var padding; // 滑块在背景中的高度
var plzd;
var minN_X;
var maxN_X;
var maxN_Y;
var minN_Y;
var imgSrc;
var offset_X; // 滑块X轴默认左偏量(等于第一次加载html的量，后续刷新不再变更)
var offset_Y; // 最大Y轴坐标
var left_Num; // 向左偏移量

var times = 0; // 用户解谜用时
var beginTime = 0; // 开始用时
var endTime = 0; // 结束用时

var imgRandomNow; // 当前图片的名称(下标 + 1)

// 添加特殊支持方法
if (!document.getElementByClassName) {

	document.getElementByClassName = function (className, index) {
		var nodes = document.getElementsByTagName("*"); //获取页面里所有元素，因为他会匹配全页面元素，所以性能上有缺陷，但是可以约束他的搜索范围；
		var arr = []; //用来保存符合的className；
		for (var i = 0; i < nodes.length; i++) {
			if (hasClass(nodes[i], className)) arr.push(nodes[i]);
		}
		if (!index) index = 0;
		return index == -1 ? arr : arr[index];
	};

	function hasClass(elem, cls) {
		cls = cls || '';
		if (cls.replace(/\s/g, '').length == 0) return false; //当cls没有参数时，返回false
		var ret = new RegExp(' ' + cls + ' ').test(' ' + elem.className + ' ');
		return ret;
	}

	function addClass(elements, cName) {
		if (!hasClass(elements, cName)) {
			elements.className += " " + cName;
		};
	}

	function removeClass(elements, cName) {
		if (hasClass(elements, cName)) {
			elements.className = elements.className.replace(new RegExp("(\\s|^)" + cName + "(\\s|$)"), " "); // replace方法是替换
		};
	}

}

var tdcode = {
	_obj: null,
	_tdcode: null,
	_img: null,
	_img_loaded: false,
	_is_draw_bg: false,
	_is_moving: false,
	_block_start_x: 0,
	_block_start_y: 0,
	_doing: false,
	_mark_w: 50,
	_mark_h: 50,
	_mark_offset: 0,
	_img_w: 240,
	_img_h: 150,
	_result: false,
	_err_c: 0,
	_onsuccess: null,

	_removeBind: function (elm, evType, fn) {
			//event.preventDefault();
			if (elm.removeEventListener) {
				elm.removeEventListener(evType, fn); //DOM2.0
				return true;
			} else if (elm.detachEvent) {
				var r = elm.detachEvent(evType, fn); //IE5+
				return r;
			}
		},
		_block_start_move: function (e) {
			//console.log("_block_start_move");

			if ((tdcode._doing || !tdcode._img_loaded) || tdcode._mark_offset != 0) { // 修复当滑块位置不是0时，禁止反复拖动滑块
				return;
			}

			beginTime = new Date(); // 用户滑动开始用时

			e.preventDefault();
			var theEvent = window.event || e;
			if (theEvent.touches) {
				theEvent = theEvent.touches[0];
			}

			var obj = document.getElementByClassName('slide_block_text');
			obj.style.display = "none";
			///tdcode._draw_bg();
			tdcode._block_start_x = theEvent.clientX;
			tdcode._block_start_y = theEvent.clientY;
			tdcode._doing = true;
			tdcode._is_moving = true;

		},
		_block_on_move: function (e) {
			// console.log("_block_on_move");

			if (!tdcode._doing || !tdcode._is_moving) {
				return;
			}

			e.preventDefault();
			var theEvent = window.event || e;
			if (theEvent.touches) {
				theEvent = theEvent.touches[0];
			}

			var now_offset = tdcode._mark_offset; // 进入移入时的offset


			tdcode._is_moving = true;

			//document.getElementById('msg').innerHTML = "move:"+theEvent.clientX+";"+theEvent.clientY;
			var offset = theEvent.clientX - tdcode._block_start_x;

			if (offset < 0) {
				offset = 0;
			}
			var max_off = tdcode._img_w - tdcode._mark_w;
			if (offset > max_off) {
				offset = max_off;
			}

			var obj = document.getElementByClassName('slide_block');
			obj.style.transform = "translate(" + offset + "px, 0px)"; // 滑动按钮跟着移动样式


			var lostbox = document.getElementByClassName('puzzle-lost-box');
			lostbox.style.transform = "translate(" + offset + "px, 0px)"; // 滑动按钮跟着移动样式

			tdcode_offset = offset; // 当前移动的坐标量
			// console.log(offset+"....");

			tdcode._mark_offset = offset / max_off * (tdcode._img_w - tdcode._mark_w);

			//tdcode._draw_bg();
			//tdcode._draw_mark();
		},
		_block_on_end: function (e) { // 滑块移动结束
			//console.log("_block_on_end");

			if (!tdcode._doing) return true;
			e.preventDefault();
			var theEvent = window.event || e;
			if (theEvent.touches) {
				theEvent = theEvent.touches[0];
			}

			endTime = new Date();
			times = ((endTime - beginTime) / 1000) % 60;
			times = Math.round(times * 10) / 10; // 保留一位小数
			times = Number(times).toFixed(1); // 如果小数位是0，则补足位数

			//console.log("总用时：" + times + "秒");

			tdcode._is_moving = false;
			tdcode._send_result();
		},
		_send_result: function () {


			tdcode._result = false;

			// 发送请求到服务器或前端实现本地处理，判断offset是否正确
			checkx(tdcode._mark_offset);
		},
		_get_result_success: function (responseText) { // 响应结果成功，此处仅代表收到响应结果，并不表示是否解谜成功
			tdcode._doing = false;
			if (responseText == true) {
				// tdcode._tdcode.innerHTML = '验证成功';

				tdcode._showmsg(tdcode._msgTimes(), 1);
				tdcode._result = true;
				document.getElementByClassName('hgroup').style.display = "block";

				setTimeout(tdcode._close, 2000);
				setTimeout(tdcode._showSuccess, 2000); // 滑块位置归0
				setTimeout(tdcode._reset, 2000); // 滑块位置归0

			} else {

				tdcode._result = false;
				tdcode._err_c++;

				// 显示抖动特效
				var obj = document.getElementById('tdcode_div');
				obj.style.display = "block";
				addClass(obj, 'tdcode_shake');
				setTimeout(function () {
					removeClass(obj, 'tdcode_shake');
				}, 200);

				// 显示提示
				tdcode._showmsg('&nbsp;&nbsp;请正确拼合图像');

				if (tdcode._err_c > 5) { // 滑动失败超过5次(即最多滑动失败6次)关闭并刷新滑块
					tdcode._close();
					tdcode._reset(); // 滑块及滑块按钮位置归0

					// 显示错误提示
					var detect = document.getElementById($elementId);

					var classVal;
					if (detect) {
						classVal = detect.className; // 原生获取元素所有class
					}

					if (classVal) { // 如果已经是错误提示就不再截取

						classVal = classVal.replace("geetest_detect", ""); // 移除class
						detect.setAttribute("class", classVal);
						classVal = classVal.concat("geetest_radar_error"); // 添加class
						detect.setAttribute("class", classVal);

						// 重新创建元素
						createOrUpdateBtn(1);

						// 之前绑定的点击元素取消方法绑定
						var initObj = document.getElementByClassName("tdcode_holder tdcode_wind");
						if (initObj) {
							tdcode._removeBind(initObj, 'touchstart', tdcode._show);
							tdcode._removeBind(initObj, 'click', tdcode._show);
						} else {
							console.log("取消绑定按钮失败！");
						}

						// 绑定重试按钮
						var retryObj = document.getElementByClassName("geetest_reset_tip_content retry");
						if (retryObj) {
							_bind(retryObj, 'touchstart', tdcode._show);
							_bind(retryObj, 'click', tdcode._show);
						} else {
							console.log("绑定重试按钮失败！");
						}
					}

				}
			}
		},
		_get_result_failure: function (xhr, status) { // 响应结果失败，可能由于网络等异常原因导致，并不代表解谜失败

		},
		_reset_slide_button: function () { // 滑块及滑块按钮位置重置

			// 滑块位置重置
			var puzzleLostBox = document.getElementByClassName('puzzle-lost-box');
			puzzleLostBox.style.transform = "translate(0px, 0px)";

			// 滑块按钮位置重置
			var slideBlock = document.getElementByClassName('slide_block');
			slideBlock.style.cssText = "transform: translate(0px, 0px)";

			setTimeout(function () { // TODO 动画效果 transition 暂未实现

				//$(".puzzle-lost-box").css({
				//"transform": 'translate(0px, 0px)',
				//"transition": "left 0.5s"
				//});
				//$(".slide_block").css({
				//"transform": 'translate(0px, 0px)',
				//"transition": "left 0.5s"
				//});
			}, 1000);


		},
		_reset: function () {

			tdcode._mark_offset = 0;

			tdcode._reset_slide_button(); // 滑块及滑块按钮位置重置

			//initHtml(); // 需要重置html滑块显示才能正常

			//draw_puzzle(); // 重新绘制解谜滑块及阴影
		},
		_refresh: function () { // 刷新按钮方法

			if (tdcode._mark_offset != 0) { // 修复当滑块位置不是0时，禁止反复拖动滑块
				return;
			}

			tdcode._reset();

			tdcode._err_c = 0;
			tdcode._is_draw_bg = false;
			tdcode._result = false;
			tdcode._img_loaded = false;

			initParam();
			initHtml();

			// 生成画布、滑块、解密空缺
			draw_bg_and_puzzle();
		},
		_resetShow: function () { // 初始显示
			tdcode._mark_offset = 0;
			var obj = document.getElementByClassName('slide_block');
			obj.style.cssText = "transform: translate(0px, 0px)";
		},
		_showSuccess: function () {

			createOrUpdateBtn(0);

			// 显示普通提示隐藏错误提示
			//var obj = document.getElementsByClassName('geetest_btn');
			//obj[0].style.display = "block"; // 显示1
			//obj[1].style.display = "none"; // 隐藏2

			// 显示验证成功动画
			document.getElementByClassName('geetest_radar_btn').style.display = "none";
			document.getElementByClassName('geetest_ghost_success geetest_success_animate').style.display = "block";

			// 验证成功不允许再次点击
			var initObj = document.getElementByClassName("tdcode_holder tdcode_wind");
			tdcode._removeBind(initObj, 'touchstart', tdcode._show);
			tdcode._removeBind(initObj, 'click', tdcode._show);

			if (tdcode._onsuccess) { // 触发验证成功回调
				tdcode._onsuccess();
			}
		},
		_show: function () {
			var obj = document.getElementByClassName('hgroup');
			if (obj) {
				obj.style.display = "none";
			}
			tdcode._refresh();
			tdcode._tdcode = this;
			document.getElementByClassName('tdcode_div_bg').style.display = "block";
			document.getElementById('tdcode_div').style.display = "block";

		},
		_close: function () { // 关闭解谜窗口

			document.getElementByClassName('tdcode_div_bg').style.display = "none";
			document.getElementById('tdcode_div').style.display = "none";

			tdcode._reset(); // 解谜重置
		},
		_showmsg: function (msg, status) {
			if (!status) {
				status = 0;
				var obj = document.getElementByClassName('tdcode_msg_error');
			} else {
				var obj = document.getElementByClassName('tdcode_msg_ok');
			}
			obj.innerHTML = msg;
			var setOpacity = function (ele, opacity) {
				if (ele.style.opacity != undefined) {
					///兼容FF和GG和新版本IE
					ele.style.opacity = opacity / 100;

				} else {
					///兼容老版本ie
					ele.style.filter = "alpha(opacity=" + opacity + ")";
				}
			};

			function fadeout(ele, opacity, speed) {
				if (ele) {
					var v = ele.style.filter.replace("alpha(opacity=", "").replace(")", "") || ele.style.opacity || 100;
					v < 1 && (v = v * 100);
					var count = speed / 1000;
					var avg = (100 - opacity) / count;
					var timer = null;
					timer = setInterval(function () {
						if (v - avg > opacity) {
							v -= avg;
							setOpacity(ele, v);
						} else {
							setOpacity(ele, 0);
							if (status == 0) {
								// 调用滑块及滑块按钮位置重置
								tdcode._reset();
							}
							clearInterval(timer);
						}

						// 拖动滑块提示文字重新显示
						if (tdcode._mark_offset == 0) {
							var objt = document.getElementByClassName('slide_block_text');
							objt.style.display = "block";
						}
					}, 100);
				}
			}

			function fadein(ele, opacity, speed) {
				if (ele) {
					var v = ele.style.filter.replace("alpha(opacity=", "").replace(")", "") || ele.style.opacity;
					v < 1 && (v = v * 100);
					var count = speed / 1000;
					var avg = count < 2 ? (opacity / count) : (opacity / count - 1);
					var timer = null;
					timer = setInterval(function () {
						if (v < opacity) {
							v += avg;
							setOpacity(ele, v);
						} else {
							clearInterval(timer);
							setTimeout(function () {
								fadeout(obj, 0, 6000);
							}, 800); // 文字提示时间
						}
					}, 100);
				}
			}

			fadein(obj, 80, 4000);
		},
		_msgTimes: function () { // 根据滑动时间返回成功超越用户提示语
			var msg = "验证成功";
			if (times) {
				if (times > 0 && times <= 1) {
					msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;99%&nbsp;的用户";
				} else if (times > 1 && times <= 2) {
					msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;98%&nbsp;的用户";
				} else if (times > 2 && times <= 3) {
					msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;88%&nbsp;的用户";
				} else if (times > 3 && times <= 4) {
					msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;49%&nbsp;的用户";
				} else if (times > 4 && times <= 5) {
					msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;22%&nbsp;的用户";
				} else if (times > 5) {
					msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;1%&nbsp;的用户";
				}
			}
			return msg;
		},
		init: function (objId, objPath) {
			$elementId = objId;
			$rootPath = objPath

			if (!tdcode._img) {

				// 仅初始化参数及页面，不绘制图片
				initParam();
				initHtml();

			}
		},
		onsuccess: function (fn) {
			tdcode._onsuccess = fn;
		}
}

function setOpacity(ele, opacity) {
	if (ele.style.opacity != undefined) {
		///兼容FF和GG和新版本IE
		ele.style.opacity = opacity / 100;

	} else {
		///兼容老版本ie
		ele.style.filter = "alpha(opacity=" + opacity + ")";
	}
}

function getCurrentUrl() {
	var list = document.getElementsByTagName('script');
	for (var i in list) {
		var d = list[i];
		if (d.src.indexOf('tn_code') !== -1) { //js文件名一定要带这个字符
			var arr = d.src.split('tn_code');
			return arr[0];
		}
	}
}

function initHtml() {

	var html = '<div class="tdcode_div_bg"></div><div class="tdcode_div" id="tdcode_div"><div class="loading" style="color: #88949D">正在加载验证码......</div>';
	html += '<canvas class="tdcode_canvas_bg"></canvas>';
	html += '<div class="puzzle-lost-box" style="position:absolute;width:' + widthConfig + 'px;height:' + heightConfig + 'px;top:0;left:0px;z-index:111;">';

	var puzzleLostHtml = '<canvas class="tdcode_canvas_puzzlelost" width="' + widthConfig + '" height="' + heightConfig + '" style="position:absolute;left:0px;z-index:33;"></canvas>';
	puzzleLostHtml += '<canvas class="tdcode_canvas_puzzleshadow" width="' + widthConfig + '" height="' + heightConfig + '" style="position:absolute;left:0px;z-index:22;"></canvas>';

	html += puzzleLostHtml + "</div>";

	html += '<canvas class="tdcode_canvas_mark" style="z-index:22;"></canvas>';
	html += '<div class="hgroup"></div>';
	html += '<div class="tdcode_msg_error"></div>';
	html += '<div class="tdcode_msg_ok"></div>';
	html += '<div class="slide"><div class="slide_block"></div><div class="slide_block_text" style="font-size:14px;width:68%;text-align:left;vertical-align:middle;">拖动滑块完成拼图</div></div><div class="tools">';
	html += '<div class="tdcode_close" title="关闭验证"></div><div class="tdcode_refresh" title="刷新验证"></div><a class="tdcode_feedback" href="javascript:void(0);"><div class="tdcode_feedback_icon" title="帮助反馈"></div></a><div class="tdcode_logo"></div>';
	html += '<div class="tdcode_tips"><a href="javascript:void(0);">由极验提供技术支持</a></div></div></div>';

	var puzzleId = "puzzle_div";

	var htmlObj = document.getElementById(puzzleId);

	if (htmlObj) {

		// 移除元素
		//htmlObj.parentNode.removeChild(htmlObj);

		var puzzleLostObj = document.getElementByClassName("puzzle-lost-box");

		puzzleLostObj.innerHTML = puzzleLostHtml;

	} else {

		// body 添加div
		htmlObj = document.createElement("div");
		htmlObj.id = puzzleId;
		htmlObj.innerHTML = html;
		document.body.appendChild(htmlObj);

		binFunc();
	}

	// 隐藏清空样式
	//var obj = document.getElementByClassName('tdcode_canvas_bg');
	//obj.style.display = "none";

	//obj = document.getElementByClassName('tdcode_canvas_mark');
	//obj.style.display = "none";

	// 显示滑块拖动描述文字
	var blockText = document.getElementByClassName('slide_block_text');
	blockText.style.display = "block";

}

/* 创建或更新btn内容 */
function createOrUpdateBtn(type) {

	var eleObj = document.getElementById($elementId);
	var btnObj = document.getElementById("tdcodeBtn");
	var btnHtml;

	if (btnObj) { // 已存在btn，先移除

		// 移除元素
		btnObj.parentNode.removeChild(btnObj);

	}

	if (type == 0) {
		// 页面创建btn元素
		btnHtml = "<div class='geetest_btn' id='tdcodeBtn'>";
		btnHtml += "<div class='geetest_radar_btn'>";
		btnHtml += "<div class='geetest_radar'>";
		btnHtml += "<div class='geetest_ring'></div>";
		btnHtml += "<div class='geetest_cross'>";
		btnHtml += "<div class='geetest_h'></div>";
		btnHtml += "<div class='geetest_v'></div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_dot'></div>";
		btnHtml += "<div class='geetest_scan'>";
		btnHtml += "<div class='geetest_h'></div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_status'>";
		btnHtml += "<div class='geetest_bg'></div>";
		btnHtml += "<div class='geetest_hook'></div>";
		btnHtml += "</div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_ie_radar'></div>";
		btnHtml += "<div class='geetest_radar_tip' tabindex='0' aria-label='点击按钮进行验证' style='outline-width: 0px;'><span>点击按钮进行验证</span><span class='geetest_reset_tip_content'>请点击重试</span><span class='geetest_radar_error_code'></span>";
		btnHtml += "</div>";
		btnHtml += "<a class='geetest_logo' href='javascript:void(0);'></a>";
		btnHtml += "<div class='geetest_offline'></div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_ghost_success geetest_success_animate' style='display:none'>";
		btnHtml += "<div class='geetest_success_btn' style='width: 100%;'>";
		btnHtml += "<div class='geetest_success_box'>";
		btnHtml += "<div class='geetest_success_show'>";
		btnHtml += "<div class='geetest_success_pie'></div>";
		btnHtml += "<div class='geetest_success_filter'></div>";
		btnHtml += "<div class='geetest_success_mask'></div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_success_correct'>";
		btnHtml += "<div class='geetest_success_icon'></div>";
		btnHtml += "</div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_success_radar_tip' style='color:#18A452'><span>验证成功</span><span class='geetest_success_radar_tip_timeinfo'></span>";
		btnHtml += "</div>";
		btnHtml += "<a class='geetest_success_logo' style='top:9px;background-position: 0 81%;' href='javascript:void(0);'></a>";
		btnHtml += "<div class='geetest_offline'></div>";
		btnHtml += "</div>";
		btnHtml += "</div>";
		btnHtml += "</div>";
	} else {
		btnHtml = "<div class='geetest_btn' id='tdcodeBtn'>";
		btnHtml += "<div class='geetest_radar_btn'>";
		btnHtml += "<div class='geetest_radar'>";
		btnHtml += "<div>";
		btnHtml += "<div class='geetest_small'></div>";
		btnHtml += "</div>";
		btnHtml += "<div style='transform: rotate(72.353deg);'>";
		btnHtml += "<div class='geetest_small'></div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_cross'>";
		btnHtml += "<div class='geetest_h'></div>";
		btnHtml += "<div class='geetest_v'></div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_dot'></div>";
		btnHtml += "<div class='geetest_scan'>";
		btnHtml += "<div class='geetest_h'></div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_status'>";
		btnHtml += "<div class='geetest_bg'></div>";
		btnHtml += "<div class='geetest_hook'></div>";
		btnHtml += "</div>";
		btnHtml += "</div>";
		btnHtml += "<div class='geetest_ie_radar'></div>";
		btnHtml += "<div class='geetest_radar_tip' tabindex='0' aria-label='网络不给力' style='outline-width: 0px;'>";
		btnHtml += "<span class='geetest_radar_tip_content'>尝试过多</span>";
		btnHtml += "<span class='geetest_reset_tip_content retry'>请点击重试</span>";
		btnHtml += "<span class='geetest_radar_error_code'>12</span>";
		btnHtml += "</div>";
		btnHtml += "<a class='geetest_logo' href='javascript:void(0);' style='top:9px !important'></a>";
		btnHtml += "<div class='geetest_other_offline geetest_offline'></div>";
		btnHtml += "</div>";
	}

	eleObj.innerHTML = btnHtml;
}

function _bind(elm, evType, fn) {
	//event.preventDefault();
	if (elm) {
		if (elm.addEventListener) {
			elm.addEventListener(evType, fn); //DOM2.0
			return true;
		} else if (elm.attachEvent) {
			var r = elm.attachEvent(evType, fn); //IE5+
			return r;
		}
	} else {
		console.log("绑定方法执行失败，elm为空！");
	}

}

function binFunc() {

	// 绑定函数
	var obj = document.getElementByClassName('slide_block');

	_bind(obj, 'mousedown', tdcode._block_start_move);
	_bind(document, 'mousemove', tdcode._block_on_move);
	_bind(document, 'mouseup', tdcode._block_on_end);

	_bind(obj, 'touchstart', tdcode._block_start_move);
	_bind(document, 'touchmove', tdcode._block_on_move);
	_bind(document, 'touchend', tdcode._block_on_end);

	var obj = document.getElementByClassName('tdcode_close');
	_bind(obj, 'touchstart', tdcode._close);
	_bind(obj, 'click', tdcode._close);

	var obj = document.getElementByClassName('tdcode_refresh');
	_bind(obj, 'touchstart', tdcode._refresh);
	_bind(obj, 'click', tdcode._refresh);


	var initObj = document.getElementById($elementId);
	_bind(initObj, 'touchstart', tdcode._show);
	_bind(initObj, 'click', tdcode._show);

	initObj.onmouseover = function (ev) { // 绑定雷达呼吸样式(对应jquery的hover)
		removeClass(initObj, 'geetest_detect');
		addClass(initObj, 'geetest_wait_compute');
	};

	initObj.onmouseout = function (ev) { // 绑定雷达呼吸样式(对应jquery的hover)
		removeClass(initObj, 'geetest_wait_compute');
		addClass(initObj, 'geetest_detect');
	};

	// $("#" + $elementId).removeClass('tdcode_holder tdcode_wind geetest_detect'); // 先移除样式，防止重复添加
	// $("#" + $elementId).addClass('tdcode_holder tdcode_wind geetest_detect'); // 添加点击按钮样式;

	removeClass(initObj, 'tdcode_holder tdcode_wind geetest_detect'); // 先移除样式，防止重复添加
	addClass(initObj, 'tdcode_holder tdcode_wind geetest_detect'); // 添加点击按钮样式;

	createOrUpdateBtn(0); // 页面生成btn元素

}

function initParam() { // 初始化公共参数

	plzd = plSize / 3;

	minN_X = padding + plSize;
	maxN_X = widthConfig - padding - plSize - plSize / 6;

	minN_Y = heightConfig - padding - plSize - plSize / 6;
	maxN_Y = padding;

	offset_X = getRand(minN_X, maxN_X); // 最大X轴坐标
	offset_Y = getRand(minN_Y, maxN_Y); // 最大Y轴坐标

}

function draw_bg_and_puzzle() { // 绘制背景图及解谜滑块

	tdcode._mark_offset = 0; // 重置滑块解

	var imgRandomNew = getRand(1, imgLibrary.length);

	while (imgRandomNow == imgRandomNew) { // 防止刷新出现相同图片
		imgRandomNew = getRand(1, imgLibrary.length);
	}

	imgRandomNow = imgRandomNew;
	imgSrc = $rootPath + "img/background/" + imgLibrary[imgRandomNew - 1] + ".png";

	tdcode._img = new Image();
	tdcode._img.src = imgSrc;
	tdcode._img.onload = function () {

		tdcode._img_loaded = true;

		// 绘制背景图
		draw_bg();

		// 绘制解谜滑块空缺及阴影
		draw_puzzlelost_2d_shadow();

		// 绘制解谜滑块及阴影
		draw_puzzle();

	};

	tdcode._img.onerror = function (error) { // 背景图加载失败
		console.log("图片路径[" + error.target.currentSrc + "]加载失败！");
	};

}

function draw_bg() {

	if (tdcode._is_draw_bg) {

		return;
	}

	tdcode._is_draw_bg = true;

	var canvas_bg = document.getElementByClassName('tdcode_canvas_bg');
	var canvas_ctx = canvas_bg.getContext('2d');
	canvas_ctx.clearRect(0, 0, canvas_bg.width, canvas_bg.height); // 清理画布
	canvas_ctx.drawImage(tdcode._img, 0, 0, tdcode._img_w, tdcode._img_h, 0, 0, tdcode._img_w, tdcode._img_h);

	obj = document.getElementByClassName('tdcode_canvas_bg');
	obj.style.display = "";
}

function draw_puzzlelost_2d_shadow() { // 生成解谜滑块空缺及阴影
	var canvas_mark = document.getElementByClassName('tdcode_canvas_mark');
	var canvas_ctx = canvas_mark.getContext("2d");
	canvas_ctx.clearRect(0, 0, canvas_mark.width, canvas_mark.height); // 清理画布

	canvas_ctx.globalCompositeOperation = "xor";
	canvas_ctx.shadowBlur = 10;
	canvas_ctx.shadowColor = "#fff"; // 滑块空缺外部阴影颜色
	canvas_ctx.shadowOffsetX = 3;
	canvas_ctx.shadowOffsetY = 3;
	canvas_ctx.fillStyle = "rgba(0,0,0,0.7)"; // 滑块空缺内部阴影颜色
	canvas_ctx.beginPath();
	canvas_ctx.lineWidth = "1";
	canvas_ctx.strokeStyle = "rgba(0,0,0,0)"; // 滑块空缺边框颜色
	canvas_ctx.moveTo(offset_X, offset_Y);
	canvas_ctx.lineTo(offset_X + plzd, offset_Y);
	canvas_ctx.bezierCurveTo(offset_X + plzd, offset_Y - plzd, offset_X + 2 * plzd, offset_Y - plzd, offset_X + 2 * plzd, offset_Y);
	canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y);
	canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y + plzd);
	canvas_ctx.bezierCurveTo(offset_X + 2 * plzd, offset_Y + plzd, offset_X + 2 * plzd, offset_Y + 2 * plzd, offset_X + 3 * plzd, offset_Y + 2 * plzd);
	canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y + 3 * plzd);
	canvas_ctx.lineTo(offset_X, offset_Y + 3 * plzd);
	canvas_ctx.closePath();
	canvas_ctx.stroke();
	canvas_ctx.fill();

	obj = document.getElementByClassName('tdcode_canvas_mark');
	obj.style.display = "";

}

function draw_puzzle() { // 生成解谜滑块及阴影


	left_Num = -offset_X + 10; // puzzle-lost-box偏移量

	var lostboxObj = document.getElementByClassName("puzzle-lost-box");

	// 修改tdcode_canvas_puzzlelost左偏移量
	lostboxObj.style.left = left_Num + "px";

	// 清空transform
	lostboxObj.style.transform = "translate(0px, 0px)";

	// 清空之前的图片

	var puzzle = document.getElementByClassName('tdcode_canvas_puzzlelost');

	var img = new Image();
	img.src = imgSrc + "?random=" + Math.random(); // 增加随机数避免浏览器缓存

	// 等待加载完成再绘制
	img.onload = function () { // 绘制滑块空缺图

		var canvas_ctx = puzzle.getContext("2d");
		canvas_ctx.clearRect(0, 0, puzzle.width, puzzle.height); // 清理画布
		canvas_ctx.beginPath();


		canvas_ctx.strokeStyle = "rgba(0,0,0,0)"; // 滑块边框颜色

		canvas_ctx.moveTo(offset_X, offset_Y);
		canvas_ctx.lineTo(offset_X + plzd, offset_Y);
		canvas_ctx.bezierCurveTo(offset_X + plzd, offset_Y - plzd, offset_X + 2 * plzd, offset_Y - plzd, offset_X + 2 * plzd, offset_Y);

		canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y);
		canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y + plzd);
		canvas_ctx.bezierCurveTo(offset_X + 2 * plzd, offset_Y + plzd, offset_X + 2 * plzd, offset_Y + 2 * plzd, offset_X + 3 * plzd, offset_Y + 2 * plzd);

		canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y + 3 * plzd);
		canvas_ctx.lineTo(offset_X, offset_Y + 3 * plzd);
		canvas_ctx.closePath();

		canvas_ctx.stroke();
		canvas_ctx.shadowBlur = 10;
		canvas_ctx.shadowColor = "black"; // 暂时无用??
		canvas_ctx.clip();

		//canvas_ctx.rotate(90 * Math.PI / 180);//顺时针旋转90°，暂时无法实现

		canvas_ctx.drawImage(img, 0, 0, widthConfig, heightConfig);

	}

	img.onerror = function (error) { // 背景图加载失败
		console.log("图片路径[" + error.target.currentSrc + "]加载失败！");
	};

	// 追加生成滑块边框阴影
	draw_puzzle_shadow();
}

function draw_puzzle_shadow() { // 追加生成滑块边框阴影

	var puzzleShadow = document.getElementByClassName('tdcode_canvas_puzzleshadow');
	var canvas_ctx = puzzleShadow.getContext("2d");
	canvas_ctx.clearRect(0, 0, puzzleShadow.width, puzzleShadow.height); // 清理画布
	canvas_ctx.beginPath();

	canvas_ctx.strokeStyle = "rgba(0,0,0,0)";
	canvas_ctx.lineWidth = "1";


	canvas_ctx.moveTo(offset_X, offset_Y);
	canvas_ctx.lineTo(offset_X + plzd, offset_Y);
	canvas_ctx.bezierCurveTo(offset_X + plzd, offset_Y - plzd, offset_X + 2 * plzd, offset_Y - plzd, offset_X + 2 * plzd, offset_Y);

	canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y);
	canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y + plzd);
	canvas_ctx.bezierCurveTo(offset_X + 2 * plzd, offset_Y + plzd, offset_X + 2 * plzd, offset_Y + 2 * plzd, offset_X + 3 * plzd, offset_Y + 2 * plzd);

	canvas_ctx.lineTo(offset_X + 3 * plzd, offset_Y + 3 * plzd);
	canvas_ctx.lineTo(offset_X, offset_Y + 3 * plzd);
	canvas_ctx.closePath();

	canvas_ctx.stroke();
	canvas_ctx.shadowBlur = 20;
	canvas_ctx.shadowColor = "black"; // 暂时无用??
	canvas_ctx.fill();
}


// 随机返回最小值及最大值的中间值
function getRand(min, max) {
	var rand = Math.random();
	return Math.round(min + rand * (max - min));
}


// 检查方法
function checkx(offset) {

	//console.log(Math.abs(tdcode_offset - offset_X));

	if ((tdcode_offset && offset_X) && (Math.abs(tdcode_offset - offset_X) <= faultx)) { // // 如果X轴滑动变量是空或便宜量，则返回，验证成功，容错2像素
		tdcode_offset = ""; // 销毁滑块值
		tdcode._get_result_success(true);
		return true;
	} else { // 验证失败
		tdcode_err++; // 错误次数累加
		if (tdcode_err > 10) { //错误10次必须刷新
			tdcode_offset = ""; // 销毁正确的坐标
		}
		tdcode._get_result_success(false);
		return false;
	}

}

function msgTimes() { // 根据滑动时间返回成功超越用户提示语
	var msg = "验证成功";
	if (times) {
		if (times > 0 && times <= 1) {
			msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;99%&nbsp;的用户";
		} else if (times > 1 && times <= 2) {
			msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;98%&nbsp;的用户";
		} else if (times > 2 && times <= 3) {
			msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;88%&nbsp;的用户";
		} else if (times > 3 && times <= 4) {
			msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;49%&nbsp;的用户";
		} else if (times > 4 && times <= 5) {
			msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;22%&nbsp;的用户";
		} else if (times > 5) {
			msg = "&nbsp;&nbsp;" + times + "&nbsp;秒的速度超过&nbsp;1%&nbsp;的用户";
		}
	}
	return msg;
}